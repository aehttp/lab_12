/* file name: kuts_lab_12_3 
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 20.01.2022
*дата останньої зміни 20.01.2022
*Лабораторна №12
*завдання : Створити та вивести одномірний масив з сум елементів кожної парної діагоналі, що паралельні головній діагоналі матриці.
*варіант : №4
*/

#include <iostream>
#include <string>
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
using namespace std;
int p_m(int *f, int nf, int mf) {
  int i, j;
    for(i = 0; i < nf; i++) 
    {
    for(j = 0; j < mf; j++) 
    printf("%5d",*(f + i*mf + j));
    cout << endl;
    }
    return 0;
  }
int i_m(int *f, int nf, int mf) {
  int i, j;
    for(i = 0; i < nf; i++) 
    {
    for(j = 0; j < mf; j++) 
    cin>>*(f + i*mf + j);
    }
  return 0;
}
//створення та виведення 
int st_v(int *f, int nf, int mf){
  int i,j,ss; 
  ss=(nf+mf)/2;
  int b[2*ss-1];
  for (int i=0;i<2*ss-1;i++)
    b[i]=0;
  for (int i=0;i<nf;i++)
    for (int j=0;j<mf;j++)
      {
        b[i-j+ss-1]+=*(f + i*mf + j);
      }
  for (int i=2*ss-2;i>=0;i--)
    cout<<b[i]<<" ";
  return 0;
}
int main()
{
  system("cls");
  SetConsoleCP(1251);
  SetConsoleOutputCP(1251);
  int n,m; 
  printf(" введіть n: ");
  cin>>n;
  printf(" введіть m: ");
  cin>>m;
  int a[n][m];
  
  printf("введіть матрицю a(%d,%d)\n",n,m);
  i_m(&a[0][0],n,m);
  system("cls");
  printf(" задана матриця: a(%d,%d)\n",n,m);
  p_m(&a[0][0],n,m);
  st_v(&a[0][0],n,m);

   system("pause");
  
return 0;
}

