/* file name: kuts_lab_12_2 
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 20.01.2022
*дата останньої зміни 20.01.2022
*Лабораторна №12
*завдання : Обчислити та вивести к-сть парних додатніх елементів масиву.
*варіант : №4
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
#include <float.h>
using namespace std; 
//функція створення та виведення елементів одновимірного масиву
int cpv(int *v, int nv)
{
	int i;
	for (i=0;i<nv;i++)
	{
     *(v+i)=(rand()%100-50);
		printf("  %5d  ",*(v+i));
	}
	cout<<"\n";
	return 0;
}
int num(int *v, int nv)
{
	int mv=0;
	int i;
	cout<<"\n";
	for (i=0;i<nv;i++)
	{
		if ((*(v+i)>0)&&((*(v+i))%2==0))
		{
		mv++;
		}
	}
	cout<<"\n";
	return mv;
	
}
int main()
{
	srand(time(NULL));
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int i, n1, n2, n3;
	cout<<" введіть розмірність трьох масивів: ";
	cin>>n1>>n2>>n3;
	int a1[n1],a2[n2],a3[n3];
	cout<<" сформований масив a1["<<n1<<"]";
	cpv(&a1[0], n1);
	cout<<" сформований масив a2["<<n2<<"]";
	cpv(&a2[0], n2);
	cout<<" сформований масив a3["<<n3<<"]";
	cpv(&a3[0], n3);
	printf(" парних додатніх в масиві a1= %5d\n", num(&a1[0],n1));
	printf(" парних додатніх в масиві a2= %5d\n", num(&a2[0],n2));
	printf(" парних додатніх в масиві a3= %5d\n", num(&a3[0],n3));
	system("pause");
	return 0;
}


