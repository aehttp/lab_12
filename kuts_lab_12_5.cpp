/* file name: kuts_lab_12_5 
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 20.01.2022
*дата останньої зміни 20.01.2022
*Лабораторна №12
*завдання : Сформувати декілька матриць. Написати логічну фукнцію, що визначає чи елементи матриці симетричні відносно головної діагоналі. 
*варіант : №4
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
#include <float.h>
using namespace std;
//ф-я , яка заповнює матрицю випадковими значеннями
void input(int *a,int n,int name, bool ran){
  int i,j;
  printf(" масив ?%d:\n",name);
  for(i=0 ; i<n ; i++){
    for(j=0; j<n ; j++){
      if(ran)
        *(a+ i*n + j)=rand()%100-50;
      printf("%4d ",*(a+ i*n + j));
    }
    cout<<endl;
  }
}
//
bool check(int *a,int n,int name){
  int i,j;
  for(i=0 ; i<n ; i++){
    for(j=0; j<n ; j++){
      if(*(a+ i*n + j) != *(a+ j*n + i)){
        printf("Елементи масиву №%d не симетричні відносно головної діагоналі \n",name);
        return false;
      }
    }
  }
  printf("Елементи масиву №%d симетричні відносно головної діагоналі \n",name);
  return true;
}
int main() {
  srand(time(NULL));
  SetConsoleCP(1251);
  SetConsoleOutputCP(1251);
  int n,m,k;
  int a[5][5],b[4][4];
  int c[3][3] = {1,2,3,2,1,2,3,2,1};
  
  input(a[0],5,1,true);
  input(b[0],4,2,true);
  input(c[0],3,3,false);
  check(a[0],5,1);
  check(b[0],4,2);
  check(c[0],3,3);
  cout<<endl;
  system("pause");
  return 0;
}

