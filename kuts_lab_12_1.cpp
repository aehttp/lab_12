/* file name: kuts_lab_12_1 
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 20.01.2022
*дата останньої зміни 20.01.2022
*Лабораторна №12
*завдання : . Розробити блок-схеми алгоритмів та реалізувати їх мовою С\С++ 
з використанням функцій для підрахунку сум та добутків. 
*варіант : №4
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
#include <float.h>
using namespace std;
double sum(int fe, int fs)
{
  double sm=0;
  int i;
  i=fs-1;
  do
  {
    i++;
    sm+=i*sin(i);
  }
  while(i<=fe);
  return sm;
}
double mul(int fe, int fs,double x)
{
  double ml=1;
  int i;
  i=fs-1;
  do
  {
    i++;
    ml*=pow(sin(x*i),2)+2*exp(-x+i);
  }
  while (i<=fe);
  return ml;
}

int main()
{
	srand(time(NULL));
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
const double  a=2.51;
double P,x;
int k_start, k_end, i_start, i_end;
cout <<" введіть початкове значення k: \t";
cin>>k_start;
cout<<" введіть кіцеве значення k: \t";
cin>>k_end;
cout<<" введіть початкове значення i: \t";
cin>>i_start;
cout<<" введіть кінцеве значенння i: \t";
cin>>i_end;
cout<<" введіть x: \t";
cin>>x;
P=sqrt(5.32*a)*sum(k_end, k_start)+mul(i_end, i_start,x);
printf(" P=%7.5f\n", P);
system ("pause");
return 0;
}

