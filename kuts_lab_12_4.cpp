/* file name: kuts_lab_12_4 
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 20.01.2022
*дата останньої зміни 20.01.2022
*Лабораторна №12
*завдання : У заданому символьному рядку підрахувати скільки разів в ньому 
зустрічаються символи "*", "!", "?" . 
*варіант : №4
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
#include <float.h>
using namespace std;
int s_k(string str, char ss){
	int i ,k=0, size = str.size();
	for (i=0; i<size ; i++){
		if (str[i] == ss)
			k++;
	}
	return k;
}
int main(){
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	string str;
	printf(" введіть рядок: ");
	getline(cin,str);
	printf("к-сть * = %4d \n",s_k(str,'*'));
	printf("к-сть ! = %4d \n",s_k(str,'!'));
	printf("к-сть ? = %4d \n",s_k(str,'?'));
	system("pause");
	return 0;
}

